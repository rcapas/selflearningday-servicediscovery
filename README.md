# README #

This repository contains a very basic implementation Service Discovery microservice using .NET Core 2.0 and Redis as storage engine. 

### Prerequisites ###

* Vagrant or Redis server
* .NET Core SDK
* Visual Studio 2017

### Setup steps ###

* Execute "vagrant up" command in the ..\Environment\ folder to launch Redis server
* Execute "dotnet run" in ..\ServiceDiscovery\ServiceDiscovery.Api\ folder to run the service or launch it through VS.
* Navigate to http://localhost:25812/swagger/ in your browser

