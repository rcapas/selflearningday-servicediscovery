﻿using StackExchange.Redis;

namespace ServiceDiscovery.Storage.Redis.Configuration
{
    public class RedisStorageConfiguration
    {
        public string[] Servers { get; set; }

        public string ClientName { get; set; }

        public string Password { get; set; }

        public ConfigurationOptions GetRedisOptions()
        {
            var configurationOptions = new ConfigurationOptions
            {
                ClientName = this.ClientName,
                Password = this.Password,
                EndPoints = { },
                AllowAdmin = true
            };

            foreach (var server in Servers)
            {
                configurationOptions.EndPoints.Add(server);
            }

            return configurationOptions;
        }
    }

}
