﻿using System.Collections.Generic;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ServiceDiscovery.DataContracts.Models;
using ServiceDiscovery.Storage.Core.Contracts;
using ServiceDiscovery.Storage.Core.DataContracts;
using ServiceDiscovery.Storage.Redis.Configuration;
using StackExchange.Redis;

namespace ServiceDiscovery.Storage.Redis
{
    public class RedisStorage : IStorage
    {
        private readonly RedisStorageConfiguration configuration;
        private readonly IConnectionMultiplexer connection;
        private const string KeyStructure = "{0}:{1}";

        public RedisStorage(IOptions<RedisStorageConfiguration> configurationOptions)
        {
            this.configuration = configurationOptions.Value;
            this.connection = ConnectionMultiplexer.Connect(this.configuration.GetRedisOptions());
        }

        public CreateOperationResponse Create(ServiceModel model)
        {
            var key = GetKey(model);
            var isSuccess = connection.GetDatabase().StringSet(key, JsonConvert.SerializeObject(model));
            var response = new CreateOperationResponse
            {
                Id = key,
                IsSuccess = isSuccess
            };

            return response;
        }

        public UpdateOperationResponse Update(ServiceModel model)
        {
            var key = GetKey(model);
            var isSuccess = connection.GetDatabase().StringSet(key, JsonConvert.SerializeObject(model));
            var response = new UpdateOperationResponse
            {
                Id = key,
                IsSuccess = isSuccess
            };

            return response;
        }

        public DeleteOperationResponse Delete(string name, string version)
        {
             var isSuccess = connection.GetDatabase().KeyDelete(GetKey(name, version));

            var response = new DeleteOperationResponse
            {
                IsSuccess = isSuccess
            };

            return response;
        }

        public GetOperationResponse Get(string name, string version)
        {
            var key = GetKey(name, version);
            var redisValue = connection.GetDatabase().StringGet(key);

            var response = new GetOperationResponse();
            if (redisValue.HasValue)
            {
                response.Result = JsonConvert.DeserializeObject<ServiceModel>(redisValue);
            }

            return response;
        }

        public GetAllOperationResponse GetAll()
        {
            var models = new List<ServiceModel>();
            var redisKeys = connection.GetServer(this.configuration.Servers[0]).Keys();

            foreach (var key in redisKeys)
            {
                var redisValue = connection.GetDatabase().StringGet(key);
                if (redisValue.HasValue)
                {
                    var model = JsonConvert.DeserializeObject<ServiceModel>(redisValue);
                    models.Add(model);
                }
            }

            var response =  new GetAllOperationResponse
            {
                Result = models.ToArray()
            };

            return response;
        }

        private static string GetKey(ServiceModel model)
        {
            return GetKey(model.Name, model.Version);
        }

        private static string GetKey(string name, string version)
        {
            return string.Format(KeyStructure, name, version);
        }
    }
}
