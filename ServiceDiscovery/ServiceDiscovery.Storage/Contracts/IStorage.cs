﻿using ServiceDiscovery.DataContracts.Models;
using ServiceDiscovery.Storage.Core.DataContracts;

namespace ServiceDiscovery.Storage.Core.Contracts
{
    public interface IStorage
    {
        CreateOperationResponse Create(ServiceModel model);

        UpdateOperationResponse Update(ServiceModel model);

        DeleteOperationResponse Delete(string name, string version);

        GetOperationResponse Get(string name, string version);

        GetAllOperationResponse GetAll();
    }
}
