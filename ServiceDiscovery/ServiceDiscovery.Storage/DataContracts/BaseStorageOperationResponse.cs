﻿namespace ServiceDiscovery.Storage.Core.DataContracts
{
    public class BaseStorageOperationResponse
    {
        public bool IsSuccess { get; set; } = true;
    }
}
