﻿namespace ServiceDiscovery.Storage.Core.DataContracts
{
    public class UpdateOperationResponse : BaseStorageOperationResponse
    {
        public string Id { get; set; }
    }
}
