﻿using ServiceDiscovery.DataContracts.Models;

namespace ServiceDiscovery.Storage.Core.DataContracts
{
    public class GetOperationResponse : BaseStorageOperationResponse
    {
        public ServiceModel Result { get; set; }
    }
}
