﻿using ServiceDiscovery.DataContracts.Models;

namespace ServiceDiscovery.Storage.Core.DataContracts
{
    public class GetAllOperationResponse : BaseStorageOperationResponse
    {
        public ServiceModel[] Result { get; set; }
    }
}
