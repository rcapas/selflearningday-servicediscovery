﻿namespace ServiceDiscovery.Storage.Core.DataContracts
{
    public class CreateOperationResponse : BaseStorageOperationResponse
    {
        public string Id { get; set; }
    }
}
