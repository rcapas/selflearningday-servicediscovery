﻿using ServiceDiscovery.DataContracts.Models;
using ServiceDiscovery.Repository.Contracts;
using ServiceDiscovery.Storage.Core.Contracts;

namespace ServiceDiscovery.Repository.Repositories
{
    public class ReadServiceRepository : IReadServiceRepository
    {
        private readonly IStorage storage;

        public ReadServiceRepository(IStorage storage)
        {
            this.storage = storage;
        }

        public ServiceModel GetService(string name, string version)
        {
            var operationResponse = storage.Get(name, version);

            return operationResponse.Result;
        } 

        public ServiceModel[] GetAllServices()
        {
            var operationResponse = storage.GetAll();

            return operationResponse.Result;
        }
    }
}
