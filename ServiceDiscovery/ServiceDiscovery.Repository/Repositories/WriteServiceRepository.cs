﻿using ServiceDiscovery.DataContracts.Models;
using ServiceDiscovery.Repository.Contracts;
using ServiceDiscovery.Storage.Core.Contracts;

namespace ServiceDiscovery.Repository.Repositories
{
    public class WriteServiceRepository : IWriteServiceRepository
    {
        private readonly IStorage storage;

        public WriteServiceRepository(IStorage storage)
        {
            this.storage = storage;
        }

        public void CreateService(ServiceModel model)
        {
            this.storage.Create(model);
        }

        public void DeleteService(string name, string version)
        {
            storage.Delete(name, version);
        }
    }
}
