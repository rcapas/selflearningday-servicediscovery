﻿using ServiceDiscovery.DataContracts.Models;

namespace ServiceDiscovery.Repository.Contracts
{
    public interface IWriteServiceRepository
    {
        void CreateService(ServiceModel model);

        void DeleteService(string name, string version);
    }
}
