﻿using ServiceDiscovery.DataContracts.Models;

namespace ServiceDiscovery.Repository.Contracts
{
    public interface IReadServiceRepository
    {
        ServiceModel GetService(string name, string version);

        ServiceModel[] GetAllServices();
    }
}
