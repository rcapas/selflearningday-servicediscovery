﻿namespace ServiceDiscovery.Commands.Contracts
{
    public interface ICommand<in TRequest, out TResponse>
    {
        TResponse Execute(TRequest request);
    }
}
