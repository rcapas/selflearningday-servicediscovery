﻿using ServiceDiscovery.Commands.Contracts;
using ServiceDiscovery.DataContracts.Models;
using ServiceDiscovery.DataContracts.Requests;
using ServiceDiscovery.DataContracts.Responses;
using ServiceDiscovery.Repository.Contracts;

namespace ServiceDiscovery.Commands.Commands
{
    public class UnRegisterServiceCommand : ICommand<UnRegisterServiceRequest, UnRegisterServiceResponse>
    {
        private readonly IWriteServiceRepository writeRepository;

        public UnRegisterServiceCommand(IWriteServiceRepository writeRepository)
        {
            this.writeRepository = writeRepository;
        }

        public UnRegisterServiceResponse Execute(UnRegisterServiceRequest request)
        {
            this.writeRepository.DeleteService(request.Name, request.Version);

            var response = new UnRegisterServiceResponse();

            return response;
        }
    }
}

