﻿using ServiceDiscovery.Commands.Contracts;
using ServiceDiscovery.DataContracts.Requests;
using ServiceDiscovery.DataContracts.Responses;
using ServiceDiscovery.Repository.Contracts;

namespace ServiceDiscovery.Commands.Commands
{
    public class GetServiceCommand : ICommand<GetServiceRequest, GetServiceResponse>
    {
        private readonly IReadServiceRepository readRepository;

        public GetServiceCommand(IReadServiceRepository readRepository)
        {
            this.readRepository = readRepository;
        }

        public GetServiceResponse Execute(GetServiceRequest request)
        {
            var service = this.readRepository.GetService(request.Name, request.Version);

            var response = new GetServiceResponse
            {
                Service = service
            };

            return response;
        }
    }
}
