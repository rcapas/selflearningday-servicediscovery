﻿using ServiceDiscovery.Commands.Contracts;
using ServiceDiscovery.DataContracts.Requests;
using ServiceDiscovery.DataContracts.Responses;
using ServiceDiscovery.Repository.Contracts;

namespace ServiceDiscovery.Commands.Commands
{
    public class GetServicesCommand : ICommand<GetServicesRequest, GetServicesResponse>
    {
        private readonly IReadServiceRepository readRepository;

        public GetServicesCommand(IReadServiceRepository readRepository)
        {
            this.readRepository = readRepository;
        }

        public GetServicesResponse Execute(GetServicesRequest request)
        {
            var services = this.readRepository.GetAllServices();

            var response = new GetServicesResponse
            {
                Services = services
            };

            return response;
        }
    }
}
