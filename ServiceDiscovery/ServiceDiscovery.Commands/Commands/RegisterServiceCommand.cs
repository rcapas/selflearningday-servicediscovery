﻿using ServiceDiscovery.Commands.Contracts;
using ServiceDiscovery.DataContracts.Models;
using ServiceDiscovery.DataContracts.Requests;
using ServiceDiscovery.DataContracts.Responses;
using ServiceDiscovery.Repository.Contracts;

namespace ServiceDiscovery.Commands.Commands
{
    public class RegisterServiceCommand : ICommand<RegisterServiceRequest, RegisterServiceResponse>
    {
        private readonly IWriteServiceRepository writeRepository;

        public RegisterServiceCommand(IWriteServiceRepository writeRepository)
        {
            this.writeRepository = writeRepository;
        }

        public RegisterServiceResponse Execute(RegisterServiceRequest request)
        {
            var serviceModel = new ServiceModel
            {
                Name = request.Name,
                Url = request.Url,
                Version = request.Version
            };

            this.writeRepository.CreateService(serviceModel);

            var response = new RegisterServiceResponse();

            return response;
        }
    }
}
