﻿namespace ServiceDiscovery.DataContracts.Models
{
    public class ServiceModel
    {
        public string Name { get; set; }

        public string Version { get; set; }

        public string Url { get; set; }
    }
}
