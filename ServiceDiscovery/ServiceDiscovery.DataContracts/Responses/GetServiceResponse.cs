﻿using ServiceDiscovery.DataContracts.Models;

namespace ServiceDiscovery.DataContracts.Responses
{
    public class GetServiceResponse
    {
        public ServiceModel Service { get; set; }
    }
}
