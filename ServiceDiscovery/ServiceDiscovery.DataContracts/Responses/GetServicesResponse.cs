﻿using ServiceDiscovery.DataContracts.Models;

namespace ServiceDiscovery.DataContracts.Responses
{
    public class GetServicesResponse
    {
        public ServiceModel[] Services { get; set; }
    }
}
