﻿namespace ServiceDiscovery.DataContracts.Requests
{
    public class GetServiceRequest
    {
        public string Name { get; set; }

        public string Version { get; set; }
    }
}
