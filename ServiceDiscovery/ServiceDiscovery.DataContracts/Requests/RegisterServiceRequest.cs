﻿namespace ServiceDiscovery.DataContracts.Requests
{
    public class RegisterServiceRequest
    {
        public string Name { get; set; }

        public string Version { get; set; }

        public string Url { get; set; }
    }
}
