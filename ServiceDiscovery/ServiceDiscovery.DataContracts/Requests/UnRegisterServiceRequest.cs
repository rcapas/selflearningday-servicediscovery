﻿namespace ServiceDiscovery.DataContracts.Requests
{
    public class UnRegisterServiceRequest
    {
        public string Name { get; set; }

        public string Version { get; set; }
    }
}
