﻿using Microsoft.AspNetCore.Mvc;
using ServiceDiscovery.Commands.Contracts;
using ServiceDiscovery.DataContracts.Requests;
using ServiceDiscovery.DataContracts.Responses;

namespace ServiceDiscovery.Api.Controllers
{
    [Route("services")]
    public class ServicesController : Controller
    {

        private readonly ICommand<GetServiceRequest, GetServiceResponse> getServiceCommand;
        private readonly ICommand<GetServicesRequest, GetServicesResponse> getServicesCommand;
        private readonly ICommand<RegisterServiceRequest, RegisterServiceResponse> registerServiceCommand;
        private readonly ICommand<UnRegisterServiceRequest, UnRegisterServiceResponse> unRegisterServiceCommand;

        public ServicesController(ICommand<GetServiceRequest, GetServiceResponse> getServiceCommand,
            ICommand<GetServicesRequest, GetServicesResponse> getServicesCommand,
            ICommand<RegisterServiceRequest, RegisterServiceResponse> registerServiceCommand,
            ICommand<UnRegisterServiceRequest, UnRegisterServiceResponse> unRegisterServiceCommand)
        {
            this.getServiceCommand = getServiceCommand;
            this.getServicesCommand = getServicesCommand;
            this.registerServiceCommand = registerServiceCommand;
            this.unRegisterServiceCommand = unRegisterServiceCommand;
        }


        [HttpGet]
        [ProducesResponseType(typeof(GetServicesResponse), 200)]
        public IActionResult GetAvailableServices()
        {
            var result = getServicesCommand.Execute(new GetServicesRequest());

            return Ok(result);
        }

        [HttpGet("{version:required}/{name:required}")]
        [ProducesResponseType(typeof(GetServiceResponse), 200)]
        [ProducesResponseType(404)]
        public IActionResult GetService(string name, string version)
        {
            var request = new GetServiceRequest
            {
                Name = name,
                Version = version
            };

            var result = getServiceCommand.Execute(request);

            if (result?.Service == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public ActionResult Register(RegisterServiceRequest request)
        {
            var result = registerServiceCommand.Execute(request);

            return StatusCode(200);
        }

        [HttpDelete]
        [ProducesResponseType(200)]
        public ActionResult UnRegister(UnRegisterServiceRequest request)
        {
            var result = unRegisterServiceCommand.Execute(request);

            return StatusCode(200);
        }

    }
}
