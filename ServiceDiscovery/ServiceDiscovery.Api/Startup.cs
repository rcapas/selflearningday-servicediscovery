﻿using System;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ServiceDiscovery.Api.Extensions;
using ServiceDiscovery.Commands.Commands;
using ServiceDiscovery.Commands.Contracts;
using ServiceDiscovery.DataContracts.Requests;
using ServiceDiscovery.DataContracts.Responses;
using ServiceDiscovery.Repository.Contracts;
using ServiceDiscovery.Repository.Repositories;
using ServiceDiscovery.Storage.Core.Contracts;
using ServiceDiscovery.Storage.Redis;
using ServiceDiscovery.Storage.Redis.Configuration;
using Swashbuckle.AspNetCore.Swagger;

namespace ServiceDiscovery.Api
{
    public class Startup
    {
        public const string CorsPolicyName = "default";
        public IContainer ApplicationContainer { get; private set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            this.Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(
                    CorsPolicyName,
                    policy =>
                    {
                        policy.AllowAnyOrigin()
                              .AllowAnyHeader()
                              .AllowAnyMethod();
                    });
            });

            services.AddMvc(setup =>
                    {
                        setup.Filters.Add(new CorsAuthorizationFilterFactory(CorsPolicyName));

                    })
                    .AddJsonOptions(jsonOpt =>
                    {
                        jsonOpt.SerializerSettings.ApplyDefault();
                    });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = Assembly.GetEntryAssembly().GetName().Name
                });
            });

            services.AddRouting();

            // Inject Configurations
            services.Configure<RedisStorageConfiguration>(Configuration.GetSection(nameof(RedisStorageConfiguration)));

            // Create the container builder.
            var builder = new ContainerBuilder();

            // Register Repositories
            builder.RegisterType<ReadServiceRepository>().As<IReadServiceRepository>();
            builder.RegisterType<WriteServiceRepository>().As<IWriteServiceRepository>();

            // Register Commands
            builder.RegisterType<GetServiceCommand>().As<ICommand<GetServiceRequest, GetServiceResponse>>();
            builder.RegisterType<GetServicesCommand>().As<ICommand<GetServicesRequest, GetServicesResponse>>();
            builder.RegisterType<RegisterServiceCommand>().As<ICommand<RegisterServiceRequest, RegisterServiceResponse>>();
            builder.RegisterType<UnRegisterServiceCommand>().As<ICommand<UnRegisterServiceRequest, UnRegisterServiceResponse>>();

            // Register Storage
            builder.RegisterType<RedisStorage>().As<IStorage>().SingleInstance();

           

            builder.Populate(services);
            this.ApplicationContainer = builder.Build();

            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvcWithDefaultRoute();

            app.UseSwagger();
            app.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v1/swagger.json", "Service Discovery"));
        }
    }
}
